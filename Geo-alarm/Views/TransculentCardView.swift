//
//  TransculentCardView.swift
//  Geo-alarm
//
//  Created by Andrew Zaiets on 06.07.17.
//  Copyright © 2017 Armadillo. All rights reserved.
//

import UIKit

class TransculentCardView: UIView {
    
    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        settings()
    }
    
    // MARK: - Privates
    
    private func settings() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        self.layer.shadowOpacity = 0.3
        self.layer.cornerRadius = 10
        self.backgroundColor = UIColor(white: 1, alpha: 0.5)
        self.layoutIfNeeded()
    }
    
}
