//
//  AlarmTableViewCell.swift
//  Geo-alarm
//
//  Created by Andrew Zaiets on 06.07.17.
//  Copyright © 2017 Armadillo. All rights reserved.
//

import UIKit

class AlarmTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = UIColor.clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

